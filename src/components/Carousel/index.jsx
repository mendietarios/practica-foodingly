import React, { useState } from "react";
import Carousel, {
  autoplayPlugin,
  slidesToShowPlugin,
  Dots,
} from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";

export default function CustomCarousel(props) {
  const { children, slides, className } = props;
  const [value, setValue] = useState(0);
  const onChange = (value) => {
    setValue(value);
  };

  return (
    <>
      <Carousel
        value={value}
        onChange={onChange}
        plugins={[
          "infinite",
          {
            resolve: autoplayPlugin,
            options: {
              interval: 5000,
            },
          },
          {
            resolve: slidesToShowPlugin,
            options: {
              numberOfSlides: slides,
            },
          },
        ]}
        breakpoints={{
          768: {
            plugins: [
              "infinite",
              {
                resolve: autoplayPlugin,
                options: {
                  interval: 5000,
                },
              },
              {
                resolve: slidesToShowPlugin,
                options: {
                  numberOfSlides: 2,
                },
              },
            ],
          },
          450: {
            plugins: [
              "infinite",
              {
                resolve: autoplayPlugin,
                options: {
                  interval: 5000,
                },
              },
              {
                resolve: slidesToShowPlugin,
                options: {
                  numberOfSlides: 1,
                },
              },
            ],
          },
        }}
        animationSpeed={1000}
        className={className}
      >
        {children}
      </Carousel>
      <Dots value={value} onChange={onChange} number={4} />
    </>
  );
}
