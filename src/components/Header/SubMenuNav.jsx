import React from "react";

const SubMenuNav = ({ items }) => {
  return (
    <div className="NavbarBottom-submenuItem">
      <span className="NavbarBottom-submenuLink">{items}</span>
    </div>
  );
};

export default SubMenuNav;
