import React, { useState } from "react";
import { Dock } from "react-dock";
import Cart from "../Cart";
import { Link } from "react-router-dom";
import DropdownComponent from "../Dropdown";
import {
  AiOutlineEllipsis,
  AiOutlineMenu,
  AiOutlineSearch,
  AiFillShopping,
} from "react-icons/ai";

export default function NavbarMobile() {
  const [isVisible, setIsVisible] = useState(false);

  const renderModal = () => (
    <Dock
      position="right"
      isVisible={isVisible}
      onVisibleChange={handleChange}
      fluid={true}
      size={0.75}
    >
      <Cart isVisible={isVisible} handleChange={handleChange} />
    </Dock>
  );

  const handleChange = () => {
    setIsVisible(!isVisible);
  };

  const iconDot = <AiOutlineEllipsis className="NavbarMobile-option" />;
  const hamburguer = <AiOutlineMenu className="NavbarMobile-option" />;
  const search = <AiOutlineSearch className="NavbarMobile-icon" />;
  const cart = (
    <i onClick={handleChange}>
      <AiFillShopping className="NavbarMobile-icon" />
    </i>
  );
  const icons = [search, cart];
  const contact = (
    <Link to="contact" className="NavbarMobile-contact">
      Contact
    </Link>
  );
  const menu = ["Home", "About us", "Food Menu", "Blog", "Pages", contact];

  return (
    <div className="NavbarMobile">
      <Link to="/">
        <div className="NavbarMobile-image" />
      </Link>
      <div className="NavbarMobile-options">
        <DropdownComponent
          elements={icons}
          selectedDefault={iconDot}
          customClass="Dropdown dots"
        />
        <DropdownComponent
          elements={menu}
          selectedDefault={hamburguer}
          customClass="Dropdown hamburguer"
        />
        {renderModal()}
      </div>
    </div>
  );
}
