import React from "react";
import {
  AiFillFacebook,
  AiFillTwitterSquare,
  AiOutlineInstagram,
  AiFillLinkedin,
} from "react-icons/ai";
import DropdownComponent from "../Dropdown";

const money = ["USD", "BD", "URO"];
const idiom = ["English", "Arabic", "French"];

const NavbarTop = () => {
  return (
    <nav className="NavbarTop">
      <div className="NavbarTop-left">
        <div className="NavbarTop-container">
          <div className="NavbarTop-social">
            <AiFillFacebook className="NavbarTop-icon" />
            <AiFillTwitterSquare className="NavbarTop-icon" />
            <AiOutlineInstagram className="NavbarTop-icon" />
            <AiFillLinkedin className="NavbarTop-icon" />
          </div>
          <div className="NavbarTop-text">
            <span className="NavbarTop-span">+001 234 567 89</span>
            <span className="NavbarTop-span">contact@domain.com</span>
          </div>
        </div>
      </div>
      <div className="NavbarTop-right">
        <DropdownComponent
          elements={idiom}
          customClass="line"
          selected="English"
        />
        <DropdownComponent elements={money} customClass="line" selected="USD" />
        <span className="NavbarTop-span">My Account</span>
      </div>
    </nav>
  );
};

export default NavbarTop;
