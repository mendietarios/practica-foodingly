import React from "react";
import NavbarBottom from "./NavbarBottom";
import NavbarMobile from "./NavbarMobile";
import NavbarTop from "./NavbarTop";

export default function Header() {
  return (
    <>
      <NavbarTop />
      <NavbarBottom />
      <NavbarMobile />
    </>
  );
}
