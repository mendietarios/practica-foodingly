import React, { useState, useContext } from "react";
import { Dock } from "react-dock";
import { AiOutlineSearch, AiFillShopping, AiOutlineDown } from "react-icons/ai";
import { Link } from "react-router-dom";
import Button from "../Button";
import SubMenuNav from "./SubMenuNav";
import Cart from "../Cart";
import AppContext from "../../context/AppProvider";

const pages = [
  "About us",
  "Reservation",
  "Chefs",
  "Testimonials",
  "FAQ",
  "User Pages",
  "Customer Dashboard",
  "Privacy Policy",
  "Terms Of Service",
  "404 Error",
];
const blogs = ["Blog", "Blog Details"];
const foodmenus = [
  "Food Grid",
  "Food Details",
  "Cart View",
  "Checkout View",
  "Order Success",
];

const NavbarBottom = () => {
  const renderPages = () =>
    pages.map((page) => <SubMenuNav key={page} items={page} />);
  const renderBlogs = () =>
    blogs.map((blog) => <SubMenuNav key={blog} items={blog} />);
  const renderFoodMenus = () =>
    foodmenus.map((foodmenu) => <SubMenuNav key={foodmenu} items={foodmenu} />);
  const [isVisible, setIsVisible] = useState(false);
  const { counter } = useContext(AppContext);

  const renderModal = () => (
    <Dock
      position="right"
      isVisible={isVisible}
      onVisibleChange={handleChange}
      fluid={true}
      size={0.17}
    >
      <Cart isVisible={isVisible} handleChange={handleChange} />
    </Dock>
  );

  const handleChange = () => {
    setIsVisible(!isVisible);
  };

  return (
    <nav className="NavbarBottom sticky">
      <a className="NavbarBottom-logo">
        <Link to="/" className="NavbarBottom-text" href="">
          <img src="https://foodingly.netlify.app/assets/img/logo.png" alt="" />
        </Link>
      </a>
      <ul className="NavbarBottom-list">
        <li>
          <Link to="/" className="NavbarBottom-text" href="">
            Home
          </Link>
        </li>
        <li>
          <a className="NavbarBottom-text" href="">
            About us
          </a>
        </li>
        <li className="NavbarBottom-item">
          <a className="NavbarBottom-text" href="">
            Food Menu <AiOutlineDown className="NavbarBottom-icon third" />
            <div className="NavbarBottom-submenu">{renderFoodMenus()}</div>
          </a>
        </li>
        <li className="NavbarBottom-item">
          <a className="NavbarBottom-text" href="">
            Blog <AiOutlineDown className="NavbarBottom-icon third" />
            <div className="NavbarBottom-submenu">{renderBlogs()}</div>
          </a>
        </li>
        <li className="NavbarBottom-item">
          <a className="NavbarBottom-text arrow" href="">
            Pages <AiOutlineDown className="NavbarBottom-icon third" />
            <div className="NavbarBottom-submenu">{renderPages()}</div>
          </a>
        </li>
        <li>
          <Link to="contact" className="NavbarBottom-text" href="">
            Contact
          </Link>
        </li>
      </ul>
      <div className="NavbarBottom-options">
        <div className="NavbarBottom-icons">
          <i onClick={handleChange}>
            <AiFillShopping className="NavbarBottom-icon" />
            <span className="NavbarBottom-counter">{counter}</span>
          </i>
          <i>
            <AiOutlineSearch className="NavbarBottom-icon second" />
          </i>
        </div>
        <Button className="Button header">Reservation</Button>
      </div>
      {renderModal()}
    </nav>
  );
};

export default NavbarBottom;
