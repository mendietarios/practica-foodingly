import React, { useContext } from "react";
import Button from "../../Button";
import AppContext from "../../../context/AppProvider";

export default function CardProduct({ product }) {
  const { id, name, image, discount, price } = product;
  const { handleAdd } = useContext(AppContext);
  const showDiscount = () => {
    if (discount) return <div className="Products-discount">{discount}%</div>;
  };

  const onAdd = () => handleAdd(id);

  return (
    <div className="Products-card">
      <div className="Products-header">
        <div
          className="Products-image"
          style={{ backgroundImage: `url(${image})` }}
        />
        {showDiscount()}
      </div>
      <div className="Products-body">
        <span className="Products-title">{name}</span>
        <div className="Products-info">
          <span className="Products-rating">4.8/5 Excellent</span>
          <span className="Products-review">(1214 reviews)</span>
        </div>
      </div>
      <div className="Products-add">
        <p className="Products-price">${price}</p>
        <div className="Products-button">
          <Button className="Button addProduct" onClick={onAdd}>
            Add To Cart
          </Button>
        </div>
      </div>
    </div>
  );
}
