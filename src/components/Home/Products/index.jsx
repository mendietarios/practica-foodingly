import React, { useEffect, useState } from "react";
import useAxios from "axios-hooks";
import Button from "../../Button";
import CardProduct from "./Card";
import Section from "../../Sections";

export default function Products() {
  const [list, setList] = useState([]);
  const [{ data }, refetch] = useAxios("/products");
  const [active, setActive] = useState("");

  useEffect(() => {
    if (!data) return;
    setList(data);
  }, [data]);

  const filterProducts = async (category) => {
    await refetch({
      params: {
        category,
      },
    });
    setActive(category);
  };

  const renderCardProducts = () =>
    list.map((product) => <CardProduct product={product} key={product.id} />);

  return (
    <Section title="Our Featured Items" subtitle="Our most popular items">
      <div className="Products-filters">
        <Button
          className={`Button products ${active === "" ? "filterActive" : ""}`}
          onClick={() => filterProducts("")}
        >
          All Categories
        </Button>
        <Button
          className={`Button products ${
            active === "NOODLES" ? "filterActive" : ""
          }`}
          onClick={() => filterProducts("NOODLES")}
        >
          Noodles
        </Button>
        <Button
          className={`Button products ${
            active === "BURGUER" ? "filterActive" : ""
          } `}
          onClick={() => filterProducts("BURGER")}
        >
          Burger
        </Button>
        <Button
          className={`Button products ${
            active === "CHICKEN" ? "filterActive" : ""
          }`}
          onClick={() => filterProducts("CHICKEN")}
        >
          Chicken
        </Button>
        <Button
          className={`Button products ${
            active === "ICE_CREAM" ? "filterActive" : ""
          }`}
          onClick={() => filterProducts("ICE_CREAM")}
        >
          Ice Cream
        </Button>
        <Button
          className={`Button products ${
            active === "DRINKS" ? "filterActive" : ""
          }`}
          onClick={() => filterProducts("DRINKS")}
        >
          Drinks
        </Button>
      </div>
      <div className="Products-list">{renderCardProducts()}</div>
    </Section>
  );
}
