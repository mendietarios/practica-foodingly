import React from "react";
import Section from "../../Sections";
import CardBlog from "./Card";
import cards from "../../../utils/cardBlog";

export default function Blog() {
  const renderCards = () =>
    cards.map((card) => <CardBlog card={card} key={card.id} />);

  return (
    <Section title="Our Blog" subtitle="Our Latest Blogs And Blogs">
      <div className="Blog">{renderCards()}</div>
    </Section>
  );
}
