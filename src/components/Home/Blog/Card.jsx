import React from "react";

const CardBlog = ({ card }) => {
  const { text, img, title, paragraph, date, user } = card;
  return (
    <div className="Blog-card">
      <div className="Blog-header">
        <span className="Blog-span">{text}</span>
        <img className="Blog-image" src={img} alt="" />
      </div>
      <div className="Blog-body">
        <span className="Blog-title">{title}</span>
        <span className="Blog-text">{paragraph}</span>
      </div>
      <div className="Blog-footer">
        <div className="Blog-date">
          <div className="Blog-info icon">
            <img
              className="Blog-icon"
              src="https://foodingly.netlify.app/assets/img/icon/cal.png"
              alt=""
            />
          </div>
          <div className="Blog-info">
            <span className="Blog-title info">Date:</span>
            <span className="Blog-text info">{date}</span>
          </div>
        </div>
        <div className="Blog-user">
          <div className="Blog-info icon">
            <img
              className="Blog-icon"
              src="https://foodingly.netlify.app/assets/img/icon/user.png"
              alt=""
            />
          </div>
          <div className="Blog-info">
            <span className="Blog-title info">By:</span>
            <span className="Blog-text info">{user}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardBlog;
