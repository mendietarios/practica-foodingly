import React from "react";

export default function DownloadApp() {
  return (
    <div className="DownloadApp">
      <div className="DownloadApp-content">
        <div className="DownloadApp-title">
          <h1 className="DownloadApp-text">Never Feel Hungry!</h1>
          <h1 className="DownloadApp-text">
            Download Our Mobile App & Enjoy Delicious Food
          </h1>
          <h1 className="DownloadApp-textmobile">
            Never Feel Hungry! Download Our Mobile App & Enjoy Delicious Food
          </h1>
        </div>
        <div className="DownloadApp-contspan">
          <span className="DownloadApp-span">
            Ut voluptate cupidatat aute et culpa sit sint occaecat ut dolor
            demon consequat eu in id. Eu ex ea commodo.
          </span>
        </div>
        <div className="DownloadApp-icons">
          <img
            className="DownloadApp-button"
            src="https://foodingly.netlify.app/assets/img/promotion/apple-store-icon.png"
            alt=""
          />
          <img
            className="DownloadApp-button"
            src="https://foodingly.netlify.app/assets/img/promotion/google-play-store-icon.png"
            alt=""
          />
        </div>
      </div>
      <div className="DownloadApp-image"></div>
    </div>
  );
}
