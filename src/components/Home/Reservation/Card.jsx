import React from "react";
import PropTypes from "prop-types";

export default function CardReservation({ card }) {
  const { title, icon, paragraph, image } = card;

  return (
    <div className="Reservation-card">
      <img className="Reservation-icon" src={icon} alt="" />
      <h1 className="Reservation-title"> {title} </h1>
      <p className="Reservation-paragraph"> {paragraph} </p>
      <img className="Reservation-image" src={image} alt="" />
    </div>
  );
}

CardReservation.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  paragraph: PropTypes.string,
  image: PropTypes.string,
};

CardReservation.defaultPropTypes = {
  title: "",
  icon: "",
  paragraph: "",
  image: "",
};
