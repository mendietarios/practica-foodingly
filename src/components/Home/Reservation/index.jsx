import React from "react";
import CardReservation from "./Card";
import Section from "../../Sections";
import Carousel from "../../Carousel";
import cards from "../../../utils/cardReservation";

export default function Reservation() {
  const renderCards = () =>
    cards.map((card) => <CardReservation key={card.id} card={card} />);

  return (
    <Section title="Our Reservation" subtitle="What we offer">
      <Carousel slides={4} className="Reservation-carousel">
        {renderCards()}
      </Carousel>
    </Section>
  );
}
