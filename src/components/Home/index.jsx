import React from "react";
import Banner from "./Banner";
import Reservation from "./Reservation";
import Offers from "./Offers";
import Products from "./Products";
import DinnerPlan from "./DinnerPlan";
import Chefs from "./Chefs";
import DownloadApp from "./DownloadApp";
import Testimonials from "./Testimonials";
import Subscribe from "./Subscribe";
import Blog from "./Blog";
import Layout from "../Layout";

export default function Home() {
  return (
    <Layout>
      <Banner />
      <Reservation />
      <Offers />
      <Products />
      <DinnerPlan />
      <Chefs />
      <DownloadApp />
      <Testimonials />
      <Subscribe />
      <Blog />
    </Layout>
  );
}
