import Button from "../../Button";
import React from "react";

export default function Offers() {
  return (
    <div className="Offers">
      <div className="Offers-heading">
        <img
          className="Offers-arrow"
          src="https://foodingly.netlify.app/assets/img/icon/arrow-left.png"
          alt=""
        />
        <h2 className="Offers-title">Special Offers</h2>
        <img
          className="Offers-arrow"
          src="https://foodingly.netlify.app/assets/img/icon/arrow-right.png"
          alt=""
        />
      </div>
      <h3 className="Offers-subtitle">
        Preferred Food, Drinks, Juice 30% Off Friday Only
      </h3>
      <Button className="Button offers">Buy Now</Button>
    </div>
  );
}
