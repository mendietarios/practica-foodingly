import React from "react";
import Button from "../../Button";
import InputForm from "../../Input";

export default function Subscribe() {
  return (
    <div className="Subscribe">
      <div className="Subscribe-icon">
        <img
          className="Subscribe-image"
          src="https://foodingly.netlify.app/assets/img/common/email.png"
          alt=""
        />
        <div className="Subscribe-text">
          <span className="Subscribe-title">
            Get the latest Blogs and offers
          </span>
          <span className="Subscribe-span">Subscribe to our Blogsletter</span>
        </div>
      </div>
      <form className="Subscribe-form">
        <InputForm
          type="text"
          className="Subscribe-input"
          placeholder="Enter your mail address"
        />
        <Button className="Button subscribe" type="button">
          Subscribe
        </Button>
      </form>
    </div>
  );
}
