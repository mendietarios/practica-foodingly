import Button from "../../Button";
import React from "react";

export default function DinnerPlan() {
  return (
    <div className="DinnerPlan">
      <div className="DinnerPlan-content">
        <h1 className="DinnerPlan-title">
          Do You Have Any Dinner Plan Today? Reserve Your Table
        </h1>
        <p className="DinnerPlan-span">
          Ut voluptate cupidatat aute et culpa sit sint occaecat ut dolor demon
          consequat eu in id. Eu ex ea commodo.
        </p>
        <Button className="Button dinnerplan">Make Reservation</Button>
      </div>
      <div className="DinnerPlan-containerImage">
        <img
          className="DinnerPlan-image"
          src="https://foodingly.netlify.app/assets/img/promotion/promo-1.png"
          alt=""
        />
      </div>
    </div>
  );
}
