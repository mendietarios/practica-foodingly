import React from "react";

const Banner = () => {
  return (
    <div className="Banner">
      <div className="Banner-container">
        <div className="Banner-content">
          <p className="Banner-text">Best dishes and ingredients</p>
          <span className="Banner-text title">
            The best options of the day in your town
          </span>
          <p className="Banner-text">
            Aliqua enim amet anim nisi minim amet veniam eu magna tempor
            laboris. Duis veniam officia culpa sunt deserunt nisi
          </p>
        </div>
        <div className="Banner-image"></div>
      </div>
    </div>
  );
};

export default Banner;
