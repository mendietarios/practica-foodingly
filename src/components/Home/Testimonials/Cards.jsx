import React from "react";
import { AiFillStar } from "react-icons/ai";

const CardTestimonials = ({ card }) => {
  const { date, img, rank, name, text } = card;

  return (
    <div className="Testimonials-cards">
      <div className="Testimonials-review">
        <span className="Testimonials-text date">{date}</span>
        <img src={img} alt="" />
        <div className="Testimonials-rank">
          <span className="Testimonials-text rank">{rank}</span>
          <div className="Testimonials-stars">
            <AiFillStar className="Testimonials-star" />
            <AiFillStar className="Testimonials-star" />
            <AiFillStar className="Testimonials-star" />
            <AiFillStar className="Testimonials-star" />
            <AiFillStar className="Testimonials-star" />
          </div>
        </div>
      </div>
      <h1 className="Testimonials-name">{name}</h1>
      <span className="Testimonials-text">{text}</span>
    </div>
  );
};

export default CardTestimonials;
