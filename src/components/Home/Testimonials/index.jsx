import React from "react";
import Section from "../../Sections";
import Carousel from "../../Carousel";
import CardTestimonials from "./Cards";
import cards from "../../../utils/cardTestimonials";

export default function Testimonials() {
  const renderCards = () =>
    cards.map((card) => <CardTestimonials key={card.id} card={card} />);

  return (
    <Section title="Testimonials" subtitle="What Our Client's Say About Us">
      <Carousel slides={3} className="Testimonials-carousel">
        {renderCards()}
      </Carousel>
    </Section>
  );
}
