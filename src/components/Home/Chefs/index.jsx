import React from "react";
import Section from "../../Sections";
import CardChefs from "./Card";
import cards from "../../../utils/cardChefs";

export default function Chefs() {
  const renderCards = () =>
    cards.map((card) => <CardChefs card={card} key={card.id} />);

  return (
    <Section title="Meet Our Chefs" subtitle="Our Experienced chefs">
      <div className="Chefs">{renderCards()}</div>
    </Section>
  );
}
