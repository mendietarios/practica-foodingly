import React from "react";
import {
  AiFillFacebook,
  AiFillTwitterSquare,
  AiOutlineInstagram,
  AiFillLinkedin,
} from "react-icons/ai";

export default function CardChefs({ card }) {
  const { img, name, job } = card;

  return (
    <div className="Chefs-card">
      <img className="Chefs-image" src={img} alt="" />
      <div className="Chefs-social">
        <div className="Chefs-icons">
          <AiFillFacebook className="Chefs-icon" />
          <AiFillTwitterSquare className="Chefs-icon" />
          <AiOutlineInstagram className="Chefs-icon" />
          <AiFillLinkedin className="Chefs-icon" />
        </div>
        <h3 className="Chefs-name">{name}</h3>
        <h4 className="Chefs-job">{job}</h4>
      </div>
    </div>
  );
}
