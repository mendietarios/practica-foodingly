import React from "react";

export default function Button({ className, onClick, children, type }) {
  return (
    <button className={className} type={type} onClick={onClick}>
      {children}
    </button>
  );
}
