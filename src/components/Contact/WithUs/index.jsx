import React from "react";
import CardContact from "./Card";
import cards from "../../../utils/cardContact";

export default function ContactWithUs() {
  const renderCards = () =>
    cards.map((card) => <CardContact key={card.id} card={card} />);

  return (
    <div className="ContactWU">
      <div className="ContactWU-top">
        <h1 className="ContactWU-title">Contact with us</h1>
      </div>
      <div className="ContactWU-center">
        <h2 className="ContactWU-text">Stay in touch</h2>
        <a className="ContactWU-text link">+00 123 456 789</a>
      </div>
      <div className="ContactWU-bottom">{renderCards()}</div>
    </div>
  );
}
