import React from "react";

const CardContact = ({ card }) => {
  const { office, place, direction } = card;

  return (
    <div className="CardContact">
      <h3 className="CardContact-heading">{office}</h3>
      <h1 className="CardContact-body">{place}</h1>
      <h2 className="CardContact-footer">{direction}</h2>
      <a className="CardContact-link" href="#">
        View on map
      </a>
    </div>
  );
};

export default CardContact;
