import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import InputFormik from "../../Input/Formik";
import Button from "../../Button";

export default function Message() {
  const schema = Yup.object({
    name: Yup.string().min(4, "Name to short").required("Name is Required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is Required"),
    phone: Yup.number()
      .positive("Must be positive number")
      .integer("Write a valid number")
      .typeError("Must be number"),
    message: Yup.string().required("Required"),
  });

  const handleSubmit = (values) => {
    alert("SEND MESSAGE");
    console.log(values);
  };

  return (
    <div className="Message">
      <h1 className="Message-title">Leave us a message</h1>
      <Formik
        initialValues={{ name: "", email: "", phone: "", message: "" }}
        onSubmit={handleSubmit}
        validationSchema={schema}
      >
        <Form className="Message-form">
          <div className="Message-input">
            <InputFormik name="name" type="text" placeholder="First Name" />
          </div>
          <div className="Message-input">
            <InputFormik
              name="email"
              type="email"
              placeholder="Email Address"
            />
          </div>
          <div className="Message-input">
            <InputFormik name="phone" type="tel" placeholder="Phone" />
          </div>
          <div className="Message-input">
            <InputFormik
              as="textarea"
              name="message"
              type="text"
              placeholder="Write message"
              customClass="message"
            />
          </div>
          <Button type="submit" className="Button subscribe">
            Send Message
          </Button>
        </Form>
      </Formik>
    </div>
  );
}
