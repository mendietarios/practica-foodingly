import React from "react";
import Layout from "../Layout";
import BannerC from "../Banner";
import ContactWithUs from "./WithUs";
import Message from "./Message";

export default function Contact() {
  return (
    <Layout>
      <BannerC page="Contact" />
      <ContactWithUs />
      <Message />
    </Layout>
  );
}
