import React from "react";

export default function InputForm({ placeholder, className, type, value }) {
  return (
    <input
      placeholder={placeholder}
      className={className}
      type={type}
      value={value}
    />
  );
}
