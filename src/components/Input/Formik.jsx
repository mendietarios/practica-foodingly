import React from "react";
import Proptypes from "prop-types";
import { Field, ErrorMessage } from "formik";

export default function InputFormik(props) {
  const { name, placeholder, as, type, customClass } = props;

  return (
    <>
      <Field
        as={as}
        name={name}
        type={type}
        placeholder={placeholder}
        className={`Message-field ${customClass}`}
      />
      <span className="Message-error">
        <ErrorMessage name={name} />
      </span>
    </>
  );
}

InputFormik.defaultProps = {
  name: "",
  customClass: "",
  as: "",
  type: "",
  placeholder: "",
};

InputFormik.propTypes = {
  as: Proptypes.string,
  name: Proptypes.string,
  type: Proptypes.string,
  customClass: Proptypes.string,
  placeholder: Proptypes.string,
};
