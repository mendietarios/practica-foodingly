import React, { useEffect, useContext, useMemo } from "react";
import Button from "../Button";
import AppContext from "../../context/AppProvider";
import { AiFillCloseCircle } from "react-icons/ai";
import { BiTrash } from "react-icons/bi";
import PropTypes from "prop-types";

export default function Cart(props) {
  const { isVisible, handleChange } = props;

  const { cart, setCart, showCart, deleteProduct } = useContext(AppContext);

  useEffect(() => {
    fetchCart();
  }, [isVisible]);

  const fetchCart = async () => {
    if (!isVisible) return;
    const cart = await showCart();
    setCart(cart.data);
  };

  const subtotal = useMemo(() => {
    let sum = 0;
    cart?.products.forEach((product) => {
      sum = sum + product.price;
    });
    return sum;
  }, [cart?.products]);

  const handleRemoveProduct = async (productId, cartId) => {
    await deleteProduct(productId, cartId);
    const cart = await showCart();
    setCart(cart.data);
  };

  const renderItems = () => {
    if (!cart?.products) {
      return <span className="Cart-void">No products in the cart </span>;
    }
    return cart?.products?.map((element) => (
      <div className="Cart-product" key={element.id}>
        <div
          className="Cart-image"
          style={{ backgroundImage: `url(${element.image})` }}
        ></div>
        <div className="Cart-info">
          <span className="Cart-title">{element.name.toLowerCase()}</span>
          <span className="Cart-price">1 x ${element.price}</span>
        </div>
        <BiTrash
          className="Cart-icon"
          onClick={() => handleRemoveProduct(element.id, cart?.id)}
        />
      </div>
    ));
  };

  return (
    <div className="Cart">
      <div className="Cart-top">
        <span className="Cart-counter">
          My Cart ({!cart?.products ? 0 : cart.products.length})
        </span>
        <AiFillCloseCircle className="Cart-close" onClick={handleChange} />
      </div>
      {renderItems()}
      <div className="Cart-total">
        <span>SUBTOTAL</span>
        <span>${subtotal}</span>
      </div>
      <Button className="Button checkout">Checkout</Button>
    </div>
  );
}

Cart.protoTypes = {
  isVisible: PropTypes.bool.isRequired,
  handleChange: PropTypes.func.isRequired,
};
