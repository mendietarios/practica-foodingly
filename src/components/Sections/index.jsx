import React from "react";
import PropTypes from "prop-types";

function Section(props) {
  const { title, subtitle, children } = props;
  return (
    <div className="Section">
      <div className="Section-heading">
        <div className="Section-top">
          <img
            className="Section-arrow"
            src="https://foodingly.netlify.app/assets/img/icon/arrow-left.png"
            alt=""
          />
          <h2 className="Section-title">{title}</h2>
          <img
            className="Section-arrow"
            src="https://foodingly.netlify.app/assets/img/icon/arrow-right.png"
            alt=""
          />
        </div>
        <h3 className="Section-subtitle">{subtitle}</h3>
        {children}
      </div>
    </div>
  );
}

Section.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Section.defaultPropTypes = {
  title: "",
  subtitle: "",
};

export default Section;
