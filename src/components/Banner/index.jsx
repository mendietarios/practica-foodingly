import React from "react";
import { FaCircle } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function BannerC(props) {
  const {page} = props
  return (
    <div className="BannerC">
      <div className="BannerC-content">
        <h1 className="BannerC-title">{page}</h1>
        <ul className="BannerC-pages">
          <li className="BannerC-item">
            <Link to="/" className="BannerC-home">
              Home
            </Link>
          </li>
          <li className="BannerC-item">
            <FaCircle className="BannerC-icon" />
          </li>
          <li className="BannerC-item">{page}</li>
        </ul>
      </div>
    </div>
  );
}
