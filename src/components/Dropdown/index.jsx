import React, { useState } from "react";
import { AiOutlineDown } from "react-icons/ai";
import PropTypes from "prop-types";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const DropdownComponent = ({
  elements,
  selected,
  selectedDefault,
  customClass,
}) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  const [value, setValue] = useState(selected);

  const handleClick = (item) => () => {
    setValue(item);
  };

  const renderItems = () =>
    elements.map((element) => (
      <DropdownItem key={element} onClick={handleClick(element)}>
        {element}
      </DropdownItem>
    ));

  return (
    <div className={`Dropdown ${customClass}`}>
      <Dropdown isOpen={dropdownOpen} toggle={toggle} direction="down">
        <DropdownToggle caret>
          {selected ? (
            <span className="Dropdown-options">
              {value} <AiOutlineDown />
            </span>
          ) : (
            selectedDefault
          )}
        </DropdownToggle>
        <DropdownMenu>{renderItems()}</DropdownMenu>
      </Dropdown>
    </div>
  );
};

DropdownComponent.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.string),
  selected: PropTypes.string.isRequired,
  selectedDefault: PropTypes.string.isRequired,
  customClass: PropTypes.string.isRequired,
};

DropdownComponent.defaultProps = {
  elements: [],
};

export default DropdownComponent;
