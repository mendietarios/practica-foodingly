import React from "react";
import FooterContent from "./FooterContent";
import Copyright from "./Copyright";

export default function Footer() {
  return (
    <>
      <FooterContent />
      <Copyright />
    </>
  );
}
