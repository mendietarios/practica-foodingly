import React from "react";
import { FaCcVisa, FaCcPaypal, FaCcJcb, FaCcMastercard } from "react-icons/fa";

export default function Copyright() {
  return (
    <div className="Copyright">
      <div className="Copyright-text">
        <span className="Copyright-span">
          Copyright © 2022 All Rights Reserved
        </span>
      </div>
      <div className="Copyright-icons">
        <i className="Copyright-icon">
          <FaCcVisa />
        </i>
        <i className="Copyright-icon">
          <FaCcPaypal />
        </i>
        <i className="Copyright-icon">
          <FaCcJcb />
        </i>
        <i className="Copyright-icon">
          <FaCcMastercard />
        </i>
      </div>
    </div>
  );
}
