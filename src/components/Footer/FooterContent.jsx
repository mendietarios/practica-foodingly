import React from "react";
import {
  AiFillFacebook,
  AiFillTwitterSquare,
  AiOutlineInstagram,
  AiFillLinkedin,
} from "react-icons/ai";

const FooterContent = () => {
  const supports = [
    "Customer Dashboard",
    "Login",
    "Cart",
    "Contact",
    "Reservation",
    "Privacy Policy",
  ];
  const links = [
    "About us",
    "Testimonials",
    "Faqs",
    "Term Service",
    "Food Grid",
    "Blog",
  ];
  const contacts = [
    {
      id: 0,
      text: "Call 24/7 for any help",
      direction: "+00 123 456 789",
    },
    {
      id: 1,
      text: "Mail to our support team",
      direction: "support@domain.com",
    },
  ];

  const Cells = [
    {
      id: 0,
      day: "Monday",
      time: "0.9.00 - 18.00",
    },
    {
      id: 1,
      day: "Tuesday",
      time: "10.00 - 18.00",
    },
    {
      id: 2,
      day: "Wednesday",
      time: "11.00 - 18.00",
    },
    {
      id: 3,
      day: "Thursday",
      time: "12.00 - 18.00",
    },
    {
      id: 4,
      day: "Thursday",
      time: "14.00 - 18.00",
    },
    {
      id: 5,
      day: "Saturday, Sunday",
      time: "Closed",
    },
  ];

  const renderSupport = () => {
    return supports.map((support) => (
      <span className="Footer-text link" key={support}>
        {support}
      </span>
    ));
  };

  const renderLinks = () => {
    return links.map((link) => (
      <span className="Footer-text link" key={link}>
        {link}
      </span>
    ));
  };

  const renderContacts = () => {
    return contacts.map((contact) => (
      <div className="Footer-content help" key={contact.id}>
        <span className="Footer-text help">{contact.text}</span>
        <a className="Footer-text url" href="">
          {contact.direction}
        </a>
      </div>
    ));
  };

  const renderCells = () => {
    return Cells.map((cell) => (
      <tr key={cell.id}>
        <th className="Footer-table cell">{cell.day}</th>
        <td className="Footer-table cell">{cell.time}</td>
      </tr>
    ));
  };

  return (
    <div className="Footer">
      <div className="Footer-container">
        <div className="Footer-row">
          <div className="Footer-column">
            <h2 className="Footer-title">Need any help?</h2>
            <div className="Footer-content">
              {renderContacts()}
              <div className="Footer-content help">
                <span className="Footer-text help">Follow us on</span>
                <div className="Footer-social">
                  <AiFillFacebook className="Footer-icon" />
                  <AiFillTwitterSquare className="Footer-icon" />
                  <AiOutlineInstagram className="Footer-icon" />
                  <AiFillLinkedin className="Footer-icon" />
                </div>
              </div>
            </div>
          </div>
          <div className="Footer-column link">
            <h2 className="Footer-title">Quick Links</h2>
            <div className="Footer-content">{renderLinks()}</div>
          </div>
        </div>
        <div className="Footer-row">
          <div className="Footer-column support">
            <h2 className="Footer-title">Support</h2>
            <div className="Footer-content">{renderSupport()}</div>
          </div>
          <div className="Footer-column hours">
            <h2 className="Footer-title">Opening hours</h2>
            <div className="Footer-content">
              <table className="Footer-table">
                <tbody>{renderCells()}</tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterContent;
