const cards = [
    {
      id: 0,
      img: "https://foodingly.netlify.app/assets/img/chefs/chefs-1.png",
      name: "Jane Cooper",
      job: "Chief chef"
    },
    {
      id: 1,
      img: "https://foodingly.netlify.app/assets/img/chefs/chefs-2.png",
      name: "Cameron Willamson",
      job: "Asst. Chef"
    },
    {
      id: 2,
      img: "https://foodingly.netlify.app/assets/img/chefs/chefs-3.png",
      name: "Brooklyn Simmons",
      job: "Asst. Chef",
    },
    {
      id: 3,
      img: "https://foodingly.netlify.app/assets/img/chefs/chefs-4.png",
      name: "Kristin Watson",
      job: "Nutritionist",
    }
  ];

export default cards;