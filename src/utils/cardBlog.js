const cards = [
    {
      id: 0,
      text: "Food & water",
      img: "https://foodingly.netlify.app/assets/img/blogs/blog-1.png",
      title: "Healthy food and nutrition among all the children",
      paragraph: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
      date: "20 Dec, 2021",
      user: "Admin",
    },
    {
      id: 1,
      text: "Delicious",
      img: "https://foodingly.netlify.app/assets/img/blogs/blog-2.png",
      title: "How to make delicious and spicy chicken grill",
      paragraph: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
      date: "20 Dec, 2021",
      user: "Admin",
    },
    {
      id: 2,
      text: "Food and nutrition",
      img: "https://foodingly.netlify.app/assets/img/blogs/blog-3.png",
      title: "Historical culture of asian food and nutrition",
      paragraph: "Lorem ipsum dolor sit amet, consectetur notted adipisicing elit sed do eiusmod...",
      date: "20 Dec, 2021",
      user: "Admin",
    },
];

export default cards;