
const cards = [
    {
      id: 0,
      icon: 'https://foodingly.netlify.app/assets/img/service/1.png',
      title: 'Breakfast',
      image: 'https://foodingly.netlify.app/assets/img/service/service-1.png',
      paragraph: 'Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.',
    },
    {
      id: 1,
      icon: 'https://foodingly.netlify.app/assets/img/service/2.png',
      title: 'Lunch',
      image: 'https://foodingly.netlify.app/assets/img/service/service-2.png',
      paragraph: 'Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.',
    },
    {
      id: 2,
      icon: 'https://foodingly.netlify.app/assets/img/service/3.png',
      title: 'Dinner',
      image: 'https://foodingly.netlify.app/assets/img/service/service-3.png',
      paragraph: 'Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.',
    },
    {
      id: 3,
      icon: 'https://foodingly.netlify.app/assets/img/service/4.png',
      title: 'Snacks',
      image: 'https://foodingly.netlify.app/assets/img/service/service-4.png',
      paragraph: 'Eu occaecat mollit tempor denim consectetur. Labore labore est du commodo veniam.',
    }
  ];

  export default cards;