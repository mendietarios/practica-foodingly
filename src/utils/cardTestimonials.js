const cards = [
    {
      id: 0,
      img: "https://foodingly.netlify.app/assets/img/review/review1.png",
      name: "Manresh Chandra",
      date: "05 Jan, 2022",
      rank: "Excellent",
      text: " “Eu ipsum ut dolore magna minim cupidatat ullamco anim sit minim irure. Consectetur voluptate nisi magna consectetur cillum proident dolore veniam voluptate adipisicing labore. Anim eiusmod dolor quis pariatur.” "
    },
    {
      id: 1,
      img: "https://foodingly.netlify.app/assets/img/review/review2.png",
      name: "Santa mariam",
      date: "08 June, 2022",
      rank: "Excellent",
      text: " “Eu ipsum ut dolore magna minim cupidatat ullamco anim sit minim irure. Consectetur voluptate nisi magna consectetur cillum proident dolore veniam voluptate adipisicing labore. Anim eiusmod dolor quis pariatur.” "
    },
    {
      id: 2,
      img: "https://foodingly.netlify.app/assets/img/review/review3.png",
      name: "Jack cremer",
      date: "22 Feb, 2022",
      rank: "Excellent",
      text: " “Eu ipsum ut dolore magna minim cupidatat ullamco anim sit minim irure. Consectetur voluptate nisi magna consectetur cillum proident dolore veniam voluptate adipisicing labore. Anim eiusmod dolor quis pariatur.” "
    },
    {
      id: 3,
      img: "https://foodingly.netlify.app/assets/img/review/review4.png",
      name: "Mandeep harshaal",
      date: "05 Jan, 2022",
      rank: "Excellent",
      text: " “Eu ipsum ut dolore magna minim cupidatat ullamco anim sit minim irure. Consectetur voluptate nisi magna consectetur cillum proident dolore veniam voluptate adipisicing labore. Anim eiusmod dolor quis pariatur.” "
    }
];

export default cards;
