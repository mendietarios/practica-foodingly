const cards = [
    {
      id: 0,
      office: "Head office",
      place: "New Mexico",
      direction: "4140 Parker Rd. Allentown, New Mexico 31134",
    },
    {
      id: 1,
      office: "Washington office",
      place: "Washington",
      direction: "4517 Washington Ave. Manchester, Kentucky 39495",
    },
    {
      id: 2,
      office: "California office",
      place: "California",
      direction: "3891 Ranchview Dr. Richardson, California 62639",
    },
    {
      id: 3,
      office: "Office schedule",
      place: "Working hours",
      direction: "Monday to Friday 9 am to 10 pm",
    },
];

export default cards;