import React from "react";
import axios from "axios";
import { configure } from "axios-hooks";
import LRU from "lru-cache";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.scss";
import Contact from "./components/Contact";
import Home from "./components/Home";
import { AppProvider } from "./context/AppProvider";

const instance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: { authorization: `Bearer ${import.meta.env.VITE_API_TOKEN}` },
});

const cache = new LRU({ max: 10 });
configure({ axios: instance, cache });

export default function App() {
  return (
    <AppProvider>
      <Router>
        <Routes>
          <Route path="contact" element={<Contact />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </Router>
    </AppProvider>
  );
}
