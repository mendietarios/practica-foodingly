import React, { createContext, useState } from "react";
import useAxios from "axios-hooks";

const AppContext = createContext({});

const AppProvider = ({ children }) => {
  const [cart, setCart] = useState(null);
  const [, refetch] = useAxios("/cart");
  const [counter, setCounter] = useState(0);

  //Creacion del Carrito
  const createCart = () =>
    refetch({
      method: "POST",
      data: { products: [] },
    });

  //Anadir producto al carrito
  const addProduct = async (productId, cartId) => {
    await refetch({
      url: "/cart/add-product",
      method: "POST",
      data: { productId, cartId },
    });
    setCounter(counter + 1);
    console.log(counter);
  };

  //Mostrar carrito
  const showCart = () => {
    return refetch({
      url: `/cart/${cart?.id}`,
      method: "GET",
    });
  };

  //Eliminar Producto
  const deleteProduct = async (productId, cartId) => {
    await refetch({
      url: "/cart/remove-product",
      method: "POST",
      data: { productId, cartId },
    });
    setCounter(counter - 1);
  };

  //Manejador de anadir
  const handleAdd = async (id) => {
    if (!cart) {
      const { data } = await createCart();
      setCart(data);
      return addProduct(id, data?.id);
    }
    return addProduct(id, cart.id);
  };

  return (
    <AppContext.Provider
      value={{
        cart,
        setCart,
        handleAdd,
        addProduct,
        showCart,
        createCart,
        deleteProduct,
        counter,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export { AppProvider };
export default AppContext;
